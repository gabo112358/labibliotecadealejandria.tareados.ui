

import bl.Autor;
import bl.Categorias;
import bl.Clientes;
import bl.Libro;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class Principal {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    private static ArrayList<Libro> listaLibros = new ArrayList<>();
    private static ArrayList<Autor> listaAutores = new ArrayList<>();
    private static ArrayList<Clientes> listaClientes = new ArrayList<>();
    private static ArrayList<Categorias> listaCategorias = new ArrayList<>();
    private static int[] listaCodigos = new int[999];

    public static void main(String[] args) throws IOException {
        menu();
    }

    public static void menu() throws IOException {
        int opcion = 0;

        do {
            System.out.println("**BIENVENIDO A LA LIBRERIA ALEJANDRO MAGNO**");
            System.out.println("1)Registro de categorias");
            System.out.println("2)Registro de libros");
            System.out.println("3)Registro de autores");
            System.out.println("4)Registro de clientes");
            System.out.println("5)Listar libros");
            System.out.println("6)Listar autores");
            System.out.println("7)Listar clientes");
            System.out.println("8)Listar categorias");
            System.out.println("9)Salir");
            System.out.println("Digite la opcion que desee:");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 9);
    }

    public static void procesarOpcion(int opcion) throws IOException {

        switch (opcion) {
            case 1:
                registroCategorias();
                break;
            case 2:
                registroLibros();
                break;
            case 3:
                registroAutores();
                break;
            case 4:
                registroClientes();
                break;
            case 5:
                listarLibros();
                break;
            case 6:
                listarAutor();
                break;
            case 7:
                listarClientes();
                break;
            case 8:
                listarCategorias();
                break;
            case 9:
                System.out.println("Ha salido correctamente");
                System.exit(0);
                break;
            default:
                System.out.println("--Opcion Invalida--");
                break;
        }
    }//fin

    public static void registroCategorias() throws IOException {
        System.out.println("Por favor ingrese el nombre de la categoria");
        String nombreCategoria = in.readLine();
        int codigo = generarCodigo();
        Categorias categoria = new Categorias(nombreCategoria, codigo);
        listaCategorias.add(categoria);
        if (listaCategorias.isEmpty()) {
            listaCategorias.add(categoria);
            System.out.println("El codigo generado para la categoria " + nombreCategoria + " es " + codigo);
        } else {
            validacionCategorias(categoria, nombreCategoria, codigo);
        }

    }// fin 

    public static void validacionCategorias(Categorias categoria, String nombreCategoria, int codigo) {
        boolean categRepetida = new Boolean(false);

        for (int i = 0; i < listaCategorias.size(); i++) {
            if (categoria.equals(listaCategorias.get(i)) || codigo == listaCategorias.get(i).getCodigo() || nombreCategoria.equals(listaCategorias.get(i).getNombreCategoria())) {
                categRepetida = true;
            }
        }
        if (categRepetida == true) {
            System.out.println("Nombre de categoria ya existente o codigo ya generado");
        } else {
            listaCategorias.add(categoria);
            System.out.println("El codigo generado para la categoria " + nombreCategoria + " es " + codigo);
        }

    }//fin 

    public static void registroLibros() throws IOException {

        System.out.println("Digite el codigo ISBN  del libro :");
        int ISBN = Integer.parseInt(in.readLine());
        System.out.println("Digite la fecha de publicacion del libro :");
        System.out.println("Digite el dia :");
        int diaPublic = Integer.parseInt(in.readLine());
        System.out.println("Digite el mes :");
        int mesPublic = Integer.parseInt(in.readLine());
        System.out.println("Digite el año :");
        int annioPublic = Integer.parseInt(in.readLine());
        LocalDate fechaPublicacion = LocalDate.of(annioPublic, mesPublic, diaPublic);
        System.out.println("Digite el nombre de la obra : ");
        String nombreObra = in.readLine();
        System.out.println("Datos la fecha de ingreso del libro");
        System.out.println("Digite el dia :");
        int diaIngreso = Integer.parseInt(in.readLine());
        System.out.println("Digite el mes :");
        int mesIngreso = Integer.parseInt(in.readLine());
        System.out.println("Digite el año :");
        int anioIngreso = Integer.parseInt(in.readLine());
        LocalDate fechaIngreso = LocalDate.of(anioIngreso, mesIngreso, diaIngreso);
        System.out.println("Digite la cantidad de paginas que posee el libro:");
        int cantidadPaginas = Integer.parseInt(in.readLine());
        System.out.println("Registro de categoría del libro :");
        String categoriaLibro = in.readLine();
        int codigo = codigoCategoria(categoriaLibro);
        Categorias categ = new Categorias(categoriaLibro, codigo);
        System.out.println("codigo de categoria " + " " + categoriaLibro + " es : " + codigo);
        System.out.println("Ingreso de  los datos del autor");
        System.out.println("Digite el nombre  del autor : ");
        String nombre = in.readLine();
        System.out.println("Digite el pais de nacimiento del autor : ");
        String pais = in.readLine();
        System.out.println("Digite la provincia de nacimiento del autor : ");
        String lugarNacimiento = in.readLine();
        System.out.println("Digite la nacionalidad del autor: ");
        String nacionalidad = in.readLine();
        System.out.println("Datos de la fecha de nacimiento del autor :");
        System.out.println("Dijite el el dia :");
        int diaNacimiento = Integer.parseInt(in.readLine());
        System.out.println("Dijite el mes :");
        int mesNacimiento = Integer.parseInt(in.readLine());
        System.out.println("Dijite el año :");
        int annioNacimiento = Integer.parseInt(in.readLine());
        LocalDate fechaNacimiento = LocalDate.of(annioNacimiento, mesNacimiento, diaNacimiento);
        int edad = edad(annioNacimiento);
        System.out.println("Edad del autor :" + edad);
        Autor datosAutor = new Autor(pais, nombre, lugarNacimiento, nacionalidad, fechaNacimiento, edad);
        Libro libros = new Libro(fechaPublicacion, nombreObra, fechaIngreso, cantidadPaginas, ISBN, datosAutor, categ);

        if (listaLibros.isEmpty()) {
            listaLibros.add(libros);
            System.out.println("Libro agregado correctamente");
        } else {
            validacionLibros(ISBN, libros);
        }

    }// fin 

    public static void validacionLibros(int ISBN, Libro libros) {

        boolean repetido = new Boolean(false);
        for (int i = 0; i < listaLibros.size(); i++) {
            if (ISBN == listaLibros.get(i).getISBN()) {
                repetido = true;
            }
        }
        if (repetido == true) {
            System.out.println("El libro no pudo ser registrado, el código ISBN ya existente");
        } else {
            listaLibros.add(libros);
            System.out.println("Libro agregado de forma correcta");
        }
    }//fin  

    public static int codigoCategoria(String categoriaLibro) {
        int codigoUno = 0;
        int codigoDos = 0;
        boolean pruebaCodigo = new Boolean(false);
        for (int i = 0; i < listaCategorias.size(); i++) {
            if (categoriaLibro.equals(listaCategorias.get(i).getNombreCategoria())) {
                codigoUno = listaCategorias.get(i).getCodigo();
                pruebaCodigo = true;
            }
        }
        if (pruebaCodigo == true) {
            codigoDos = codigoUno;

        } else {
            System.out.println("La categoria  :" + categoriaLibro + " aun no ha sido registrada "
                    + "por el administrador, el codigo es : " + codigoDos);
        }
        return codigoDos;

    }//fin

    public static int edad(int anioNacimiento) {
        int anioActual = 2021;
        int edad = anioActual - anioNacimiento;
        return edad;
    }

    public static void listarLibros() {
        for (int i = 0; i < listaLibros.size(); i++) {
            System.out.println(listaLibros.get(i).toString());
        }
    }//fin

    public static void registroAutores() throws IOException {
        System.out.println("Digite el pais de procedencia del autor : ");
        String pais = in.readLine();
        System.out.println("Digite la identificacion del autor en la biblioteca  :");
        String id = in.readLine();
        System.out.println("Digite el nombre completo del autor  :");
        String nombre = in.readLine();
        System.out.println("Digite la provincia de nacimiento : ");
        String lugarNacimiento = in.readLine();
        System.out.println("Digite la nacionalidad del autor: ");
        String nacionalidad = in.readLine();
        System.out.println("Datos de la fecha de nacimiento del autor :");
        System.out.println("Digite el dia :");
        int diaNacimiento = Integer.parseInt(in.readLine());
        System.out.println("Digite el mes :");
        int mesNacimiento = Integer.parseInt(in.readLine());
        System.out.println("Digite el año :");
        int annioNacimiento = Integer.parseInt(in.readLine());
        LocalDate fechaNacimiento = LocalDate.of(annioNacimiento, mesNacimiento, diaNacimiento);
        int edad = edad(annioNacimiento);
        System.out.println("Edad del autor :" + edad);

        Autor datosAutor = new Autor(pais, id, nombre, lugarNacimiento, nacionalidad, fechaNacimiento, edad);

        if (listaAutores.isEmpty()) {
            listaAutores.add(datosAutor);
            System.out.println("Datos de autor agregado correctamente");
        } else {
            validarAutor(datosAutor, id);
        }
    }//fin

    public static void validarAutor(Autor datosAutor, String id) {
        boolean autorRepetido = new Boolean(false);
        for (int x = 0; x < listaAutores.size(); x++) {
            if (id.equals(listaAutores.get(x).getIdentificacion())) {
                autorRepetido = true;
            }
        }

        if (autorRepetido == true) {
            System.out.println("El autor con la identificacion: " + id + " ya existe");
        } else {
            listaAutores.add(datosAutor);
            System.out.println("Datos de autor agregado correctamente");
        }

    }//fin

    public static void listarAutor() {
        for (int x = 0; x < listaAutores.size(); x++) {
            System.out.print(listaAutores.get(x).toString() + "\n");
        }
    }//fin     

    public static void registroClientes() throws IOException {
        System.out.println("Digite el pais de procedencia del cliente : ");
        String paisCliente = in.readLine();
        System.out.println("Digite la identificacion del cliente  :");
        String identificacion = in.readLine();
        System.out.println("Digite el nombre completo del cliente  :");
        String nombreCliente = in.readLine();
        System.out.println("Digite el nombre del libro que el cliente se va llevar ");
        String nombreLibro = in.readLine();
        Libro libroCliente = new Libro(nombreLibro);
        //objeto Cliente
        Clientes cliente = new Clientes(paisCliente, identificacion, nombreCliente, libroCliente);
        if (listaClientes.isEmpty()) {
            listaClientes.add(cliente);
            System.out.println("Cliente agregado correctamente");
        } else {
            validarCliente(identificacion, cliente);
        }
    }//fin

    public static void validarCliente(String identificacion, Clientes cliente) {
        boolean clienteRepetido = new Boolean(false);
        for (int i = 0; i < listaClientes.size(); i++) {
            if (identificacion.equals(listaClientes.get(i).getIdentificacion())) {
                clienteRepetido = true;
            }
        }

        if (clienteRepetido == true) {
            System.out.println("El cliente con la identificacion: " + identificacion + "  ya existe");
        } else {
            listaClientes.add(cliente);
            System.out.println("Cliente agregado correctamente");
        }

    }//fin  

    public static void listarClientes() {
        for (int i = 0; i < listaClientes.size(); i++) {
            System.out.println(listaClientes.get(i) + " ,"
                    + " libro escogido = "
                    + listaClientes.get(i).getNombreLibro());
        }
    }// fin

    public static void listarCategorias() {
        for (int i = 0; i < listaCategorias.size(); i++) {
            System.out.println(listaCategorias.get(i));
        }
    }//fin

    public static int generarCodigo() {
        int codigo = (int) (Math.random() * 999 + 1);
        for (int i = 0; i < listaCodigos.length; i++) {
            if (listaCategorias.isEmpty()) {
                if (codigo == listaCodigos[i]) {
                    i = listaCodigos.length;
                }
            } else {
                listaCodigos[i] = codigo;
                i = listaCodigos.length;;
            }
        }
        return codigo;
    }//fin  

}//fin
